﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Pocisk po X czasie exploduje, a w jego miejsce tworzy się nowa wieza
public class ExplodeBullet : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Explode());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //explode
    IEnumerator Explode()
    {
        yield return new WaitForSeconds(1);
        Destroy(gameObject);
    }
}
